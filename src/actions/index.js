import axios from 'axios';

const API_KEY ='1f59f0003364e9a6a1e5d82bd471838e';
const ROOT_URL =  `http://api.openweathermap.org/data/2.5/forecast?us&mode=&appid=${API_KEY}`;
export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city){
  const url = `${ROOT_URL}&q=${city},us`;
  const request = axios.get(url); //return promise
  console.log('req',request);

  return {
    type: FETCH_WEATHER,
    payload: request
  };
}
