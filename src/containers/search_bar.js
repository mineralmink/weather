import React,{ Component } from 'react';
import  { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class Searchbar extends Component {
  constructor(props){
    super(props);

    this.state = { term: ''};
    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    console.log(event.target.value);
    this.setState({term: event.target.value});
  }
  onFormSubmit(event) {
    event.preventDefault();
    //We need to go and fetch weather data
    this.props.fetchWeather(this.state.term);
    this.setState({term:''})
    //clear term input and rerender value
  }
  render(){
    return(
      <form onSubmit={this.onFormSubmit} className="input-group">
        <input
          placeholder ="Get a five-day forecast in your favorite cities"
          className="form-control"
          value={this.state.term}
          onChange ={this.onInputChange} />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">Submit</button>
        </span>
      </form>
    );
  }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchWeather }, dispatch);
    //actioncreator and dispatch is to make sure that action flow down into middleware and reducer
}

export default connect(null, mapDispatchToProps)(Searchbar);
//pass null for first argument (mapStateToProps) and containers don't even care
