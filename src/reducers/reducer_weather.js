import { FETCH_WEATHER } from '../actions/index';
export default function(state = [],action) {
  switch (action.type){
    case FETCH_WEATHER:
      console.log('action data',action.payload);
      return state.concat([action.payload.data]);
      //new instance of state or we write return [ action.payload.data, ...state]; ... it means crate new array
  }
  return state;
}
